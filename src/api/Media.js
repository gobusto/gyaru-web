import apiCall from './apiCall.js'
import buildGetQuery from './buildGetQuery.js'
import buildFormData from './buildFormData.js'

// Handles all media requests.
class Media {
  static all (params) {
    const data = buildGetQuery(params)
    return apiCall('GET', 'api/v1/media', null, data)
  }

  static show (id) {
    return apiCall('GET', `api/v1/media/${id}`)
  }

  static create (token, params) {
    const data = buildFormData('media', params)
    return apiCall('POST', 'api/v1/media', token, data)
  }

  static update (token, id, params) {
    const data = buildFormData('media', params)
    return apiCall('PATCH', `api/v1/media/${id}`, token, data)
  }

  static destroy (token, id) {
    return apiCall('DELETE', `api/v1/media/${id}`, token)
  }

  static associatedMedia (id) {
    return apiCall('GET', `api/v1/media/${id}/associated_media`)
  }
}

export default Media
