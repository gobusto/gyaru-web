import React from 'react'

import EditReviewForm from '../../components/EditReviewForm/EditReviewForm.js'

import ReviewApi from '../../api/Review.js'

// writeme
class CreateReviewPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (params) {
    ReviewApi.create(this.props.token, params).then(
      json => this.props.onPageChange('reviews', json.id),
      this.props.onError
    )
  }

  render () {
    const e = React.createElement

    return e('div', { className: 'create-review-page' },
      e(EditReviewForm, {
        mediaId: this.props.id,
        onSubmit: this.handleSubmit,
        onCancel: () => this.props.onPageChange('media', this.props.id)
      })
    )
  }
}

export default CreateReviewPage
