import React from 'react'

import EditCharacterForm from '../../components/EditCharacterForm/EditCharacterForm'
import PleaseWait from '../../components/PleaseWait/PleaseWait'

import CharacterApi from '../../api/Character'
import MediaApi from '../../api/Media'

// writeme
class CreateCharacterPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = { character: null }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    MediaApi.show(this.props.id).then(
      json => {
        const newCharacter = { associatedCharacters: [{ media: json }] }
        this.setState({ character: newCharacter })
      },
      this.props.onError
    )
  }

  handleSubmit (params) {
    CharacterApi.create(this.props.token, params).then(
      json => this.props.onPageChange('characters', json.id),
      this.props.onError
    )
  }

  render () {
    if (!this.state.character) { return <PleaseWait /> }

    return (
      <div className='edit-character-page'>
        <EditCharacterForm
          character={this.state.character}
          onSubmit={this.handleSubmit}
          onCancel={() => this.props.onPageChange('media', this.props.id)}
        />
      </div>
    )
  }
}

export default CreateCharacterPage
