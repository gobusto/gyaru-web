// Takes a list of strings and converts them into a sentence, like Rails does.
function toSentence (words) {
  return words.map((word, idx) => (
    idx ? `${idx < words.length - 1 ? ', ' : ' and '} ${word}` : word
  )).join('')
}

export default toSentence
