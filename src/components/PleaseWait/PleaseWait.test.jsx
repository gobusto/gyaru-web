import React from 'react'
import { render } from '@testing-library/react'
import PleaseWait from './PleaseWait'

it('renders correctly', () => {
  const result = render(<PleaseWait />)

  expect(result).toMatchSnapshot()
})
