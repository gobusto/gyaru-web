import React from 'react'
import './RecentMedia.css'

import PleaseWait from '../PleaseWait/PleaseWait'
import RecentMediaItem from '../RecentMediaItem/RecentMediaItem'

import MediaApi from '../../api/Media'

// writeme
class RecentMedia extends React.Component {
  constructor (props) {
    super(props)

    this.state = { items: null }

    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount () {
    const params = {
      kind: this.props.kind || '',
      sortField: 'dateAdded',
      sortOrder: 'DESC',
      limit: 4
    }

    MediaApi.all(params).then(
      json => this.setState({ items: json.items }),
      this.props.onError
    )
  }

  handleClick (item) {
    this.props.onPageChange('media', item.id)
  }

  render () {
    const items = this.state.items ? this.state.items.map(item => (
      <RecentMediaItem
        key={item.id}
        media={item}
        onClick={() => this.handleClick(item)}
      />
    )) : (
      <PleaseWait />
    )

    return (
      <div className='recent-media'>
        <div className='recent-media-title'>
          Recently-added {this.props.kind || 'media'}
        </div>
        <div className='recent-media-items'>
          {items}
        </div>
      </div>
    )
  }
}

export default RecentMedia
