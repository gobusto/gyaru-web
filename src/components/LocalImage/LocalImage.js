import React from 'react'

// If we don't control the URL path, assume images are relative to this file:
function LocalImage (props) {
  const prefix = process.env.REACT_APP_PATHLESS_ROUTING ? '' : '/'
  return React.createElement('img', { ...props, src: `${prefix}${props.src}` })
}

export default LocalImage
