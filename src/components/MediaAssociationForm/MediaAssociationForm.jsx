import React from 'react'
import './MediaAssociationForm.css'

import InputLabel from '../InputLabel/InputLabel'
import SimpleButton from '../SimpleButton/SimpleButton'
import SimpleDropdown from '../SimpleDropdown/SimpleDropdown'
import SimpleInput from '../SimpleInput/SimpleInput'

import { associatedMediaKinds } from '../../helpers/commonEnums'
import localName from '../../helpers/localName'

// writeme
function MediaAssociationForm (props) {
  if (props.association.destroy) { return null }

  const kind = props.association.inverse ? (
    <SimpleInput
      value={props.association.kind}
      title='This is an auto-generated ("inverse") association.'
      disabled={true}
    />
  ) : (
    <SimpleDropdown
      value={props.association.kind}
      options={associatedMediaKinds}
      onChange={props.onChange}
    />
  )

  return (
    <div className='media-association-form'>
      <div className='media-association-form-kind'>
        <InputLabel label='Association'>
          {kind}
        </InputLabel>
      </div>
      <div className='media-association-form-media'>
        <InputLabel label='Other media'>
          <SimpleInput
            value={localName(props.association.media)}
            disabled={true}
          />
        </InputLabel>
      </div>
      <div className='media-association-form-remove'>
        <SimpleButton theme='delete' onClick={props.onRemove} disabled={props.association.inverse}>
          Remove
        </SimpleButton>
      </div>
    </div>
  )
}

export default MediaAssociationForm
