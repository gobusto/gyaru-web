import React from 'react'
import './PageFooter.css'

// writeme
function PageFooter (props) {
  const e = React.createElement

  // The FAQ and Rules pages are relative to the base URL, not the current one:
  const prefix = process.env.REACT_APP_PATHLESS_ROUTING ? '.' : ''

  const adminActions = (props.currentUser || {}).administrator ? (
    e(React.Fragment, null,
      e('button', {
        className: 'page-footer-create-media',
        onClick: () => props.onPageChange('create-media')
      }, 'Add media'),
      e('button', {
        className: 'page-footer-create-staff',
        onClick: () => props.onPageChange('create-staff')
      }, 'Add staff'),
      e('button', {
        className: 'page-footer-create-studio',
        onClick: () => props.onPageChange('create-studio')
      }, 'Add studio'),
      e('button', {
        className: 'page-footer-manage-tags',
        onClick: () => props.onPageChange('tags')
      }, 'Manage tags')
    )
  ) : null

  return (
    e('div', { className: 'page-footer' },
      adminActions,
      e('a', { href: `${prefix}/rules.html`, target: '_blank' }, 'Rules'),
      e('a', { href: `${prefix}/faq.html`, target: '_blank' }, 'FAQ'),
      e('button', {
        onClick: () => props.onPageChange('moderators')
      }, 'Moderators'),
      e('a', {
        href: 'https://opensource.org/licenses/MIT',
        target: '_blank'
      }, 'Licence'),
      e('a', {
        href: 'https://gitlab.com/gobusto/gyaru-web',
        target: '_blank'
      }, 'Source')
    )
  )
}

export default PageFooter
